// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

	let studGrade = (grade) => {
		if (grade < 74){
			console.log(`Failed. Your quarterly average is ${grade}.`);
		} else if (grade > 74 && grade <= 80){
			console.log(`Beginner. Your quarterly average is ${grade}.`);
		} else if (grade > 80 && grade <= 85){
			console.log(`Congratulations! Your quarterly average is ${grade}. You have received a Developing" mark.`);
		}else if (grade > 85 && grade <= 90){
			console.log(`Above Average. Your quarterly average is ${grade}.`);
		}else if (grade > 90 && grade <= 100){
			console.log(`Advanced. Your quarterly average is ${grade}.`);
		}else{
			console.log("Unknown Grade Value");
		}
	}

	studGrade(86);

/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/
	for(let i = 1; i <= 300; i++){
		if(i % 2 == 0){
		console.log(`${i} - even`);
	}else{
		console.log(`${i} - odd`);
	}
}


/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/

	let object = {
		heroName: prompt("Input Hero Name"),
		origin: prompt("Input Origin"),
		description: prompt("Input Description"),
		skills: {
			skill1: prompt("Input Skill 1"),
			skill2: prompt("Input Skill 2"),
			skill3: prompt("Input Skill 3")
		}
	}

console.log(object);